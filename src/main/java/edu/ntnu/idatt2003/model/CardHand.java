package edu.ntnu.idatt2003.model;

import java.util.ArrayList;

/**
 * A class representing a hand of cards from @see PlayingCard.
 * It includes methods to check the results of the hand (such as checking for a queen of spades)
 */

public class CardHand {
  ArrayList<PlayingCard> hand;
  
  /**
   * Initializes a ArrayList to hold new hand of cards
   */
  public CardHand() {
    hand = new ArrayList<>();
  }
  
  /**
   * Returns the hand of cards
   *
   * @return the hand of cards
   */
  public ArrayList<PlayingCard> getHand() {
    return hand;
  }
  
  /**
   * Adds an individual card to the hand
   * Used in @see DeckOfCards to deal a hand of cards
   *
   * @param card the card to add to the hand
   */
  public void receiveCard(PlayingCard card) throws IllegalArgumentException {
    if (card == null) {
      throw new IllegalArgumentException("Card cannot be null");
    }
    getHand().add(card);
  }
  
  /**
   * Calculates the sum of the face values of the cards in the hand
   *
   * @return the sum of the face values of the cards in the hand
   */
  public String sumCards() {
    return String.valueOf(getHand()
        .stream().mapToInt(PlayingCard::getFace).sum());
  }
  
  /**
   * Shows a string of all the hearts in the hand, if any
   *
   * @return a string of all the hearts in the hand
   * if there are no hearts, returns a string saying "No hearts in hand"
   */
  public String showHearts() {
    StringBuilder hearts = new StringBuilder();
    StringBuilder finalHearts = hearts;
    
    getHand().stream()
        .filter(card -> card.getSuit() == 'H')
        .forEach(card -> finalHearts.append(card.getAsString()).append(" | "));
    
      if (hearts.isEmpty()) {
        hearts = new StringBuilder("No hearts in hand");
      }
    return hearts.toString();
    }
  
  /**
   * Checks if there is a queen of spades (S12) in the hand
   *
   * @return "Yes" if the hand contains the queen of spades, "No" otherwise
   */
  public String checkForSpadeQueen() {
    if (getHand().stream().
        filter(card -> card.getFace() == 12 && card.getSuit() =='S')
        .collect(ArrayList::new, ArrayList::add, ArrayList::addAll).size() == 1) {
      return "Yes";
    }
    return "No";
  }
  
  /**
   * Checks if the hand is a flush (all cards have the same suit)
   *
   * @return "Yes" if the hand is a flush, "No" otherwise
   */
  public String checkForFlush() {
    char suit = getHand().getFirst().getSuit();
    if (getHand().size() == getHand()
        .stream().filter(card -> card.getSuit() == suit)
        .collect(ArrayList::new, ArrayList::add, ArrayList::addAll).size()) {
      return "Yes";
    }
    return "No";
  }
}
