package edu.ntnu.idatt2003.model;

import java.util.ArrayList;
import java.util.Random;

/**
 * A class representing a deck of cards, each of the type @see PlayingCard
 * The deck is initialized with 52 cards, 13 of each suit
 * A hand of cards can be dealt from the deck and initialized as a @see CardHand
 */
public class DeckOfCards {
  private final char[] suit = {'S', 'H', 'D', 'C'};
  private final int[] face = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
  Random random = new Random();
  ArrayList<CardHand> hands = new ArrayList<>();
  private final ArrayList<PlayingCard> cardDeck = new ArrayList<>();
  
  /**
   * Initializes a deck of cards with 52 cards, 13 of each suit
   */
  public DeckOfCards() {
    for (char suit : suit) {
      for (int face : face) {
        PlayingCard card = new PlayingCard(suit, face);
        cardDeck.add(card);
      }
    }
  }
  
  /**
   * Returns the deck of cards
   *
   * @return the deck of cards
   */
  public ArrayList<PlayingCard> getCardDeck() {
    return cardDeck;
  }
  
  /**
   * Returns a card from the deck at the given index
   *
   * @param n the index of the card to return
   * @return the card at the given index
   */
  public PlayingCard selectCard(int n) {
    return cardDeck.get(n);
  }
  
  /**
   * Removes a card from the deck at the given index
   *
   * @param n the index of the card to remove
   */
  public void removeCard(int n) {
    cardDeck.remove(n);
  }
  
  /**
   * Deals a hand of cards from the deck and initializes a new @see CardHand
   * Removes those cards from the deck
   * saves the CardHand object in an ArrayList
   *
   * @param n the number of cards to deal
   */
  public void dealHand(int n) {
    if (n < 0 || n > getCardDeck().size()) {
      throw new IllegalArgumentException("There are not enough cards in the deck");
    } else if (n == 0) {
      throw new IllegalArgumentException("Cannot deal zero cards");
    }
    CardHand cardHand = new CardHand();
    for (int i = 0; i < n; i++) {
      int index = random.nextInt(getCardDeck().size());
      cardHand.receiveCard(selectCard(index));
      removeCard(index);
    }
    hands.add(cardHand);
  }
  
  /**
   * Returns a String representation of the latest hand that was dealt
   *
   * @return the latest hand that was dealt
   */
  public String showHand() {
    StringBuilder hand = new StringBuilder();
    for (PlayingCard card : getLatestHand().getHand()) {
      hand.append(card.getAsString()).append(" | ");
    }
    return hand.toString();
  }
  
  /**
   * Returns the latest hand that was dealt
   *
   * @return the latest hand that was dealt
   */
  public CardHand getLatestHand() throws IllegalArgumentException{
    if (hands.isEmpty()) {
      throw new IllegalArgumentException("No hands have been dealt");
    }
   return hands.getLast();
  }
  
}