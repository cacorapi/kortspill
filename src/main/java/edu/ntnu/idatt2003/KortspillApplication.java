package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.model.DeckOfCards;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * A class representing the application for the card game
 * It includes a GUI written in javafx for dealing and checking a hand of cards
 */

public class KortspillApplication extends Application {
  TextArea cardHandDisplay = new TextArea();
  TextArea sumTextArea = new TextArea();
  TextArea heartsTextArea = new TextArea();
  TextArea flushTextArea = new TextArea();
  TextArea queenTextArea = new TextArea();
  DeckOfCards cardDeck;
  
  /**
   * The main method of the application
   *
   * @param args the arguments given to the application
   */
  public static void main(String[] args) {
    launch(args);
  }
  
  /**
   * Starts the application
   *
   * @param primaryStage the stage to start the application in
   */
  @Override
  public void start(Stage primaryStage) {
    primaryStage.setTitle("Kortspill");
    BorderPane root = new BorderPane();
    
    AnchorPane.setTopAnchor(cardHandDisplay, 16.0);
    AnchorPane.setLeftAnchor(cardHandDisplay, 20.0);
    cardHandDisplay.setStyle("-fx-font-size: 24px;");
    cardHandDisplay.setMaxSize(350,100);
    cardHandDisplay.setEditable(false);
    root.setCenter(cardHandDisplay);
    
    GridPane handResultsDisplay = createHandResultsDisplay();
    VBox buttons = createButtonDisplay();
    
    root.setBottom(handResultsDisplay);
    root.setRight(buttons);
    
    primaryStage.setScene(new Scene(root, 300, 250));
    primaryStage.show();
  }
  
  /**
   * Creates a HBox with a label and a text area to be used for the hand results in the GridPane
   *
   * @param label the label to display
   * @param textArea the text area to display
   * @return a HBox with the label and text area
   */
  private HBox createHBox(String label, TextArea textArea) {
    HBox hbox = new HBox();
    hbox.setAlignment(Pos.CENTER);
    hbox.setSpacing(10);
    textArea.setEditable(false);
    hbox.getChildren().addAll(new Label(label), textArea);
    return hbox;
  }
  
  /**
   * Creates a GridPane with the results of the hand
   *
   * @return a GridPane with the results of the hand
   */
  private GridPane createHandResultsDisplay() {
    GridPane cardHandResults = new GridPane();
    cardHandResults.setPadding(new Insets(5, 5, 5, 5));
    cardHandResults.setVgap(10);
    
    sumTextArea.setPrefSize(50, 25);
    heartsTextArea.setPrefSize(140, 25);
    flushTextArea.setPrefSize(50, 25);
    queenTextArea.setPrefSize(50, 25);
    
    cardHandResults.add(createHBox("Sum of the faces:", sumTextArea), 1, 0);
    cardHandResults.add(createHBox("Cards of hearts:", heartsTextArea), 9, 0);
    cardHandResults.add(createHBox("Flush:", flushTextArea), 1, 1);
    cardHandResults.add(createHBox("Queen of spades:", queenTextArea), 9, 1);
    
    return cardHandResults;
  }
  
  /**
   * Creates a VBox with buttons for dealing and checking a hand of cards
   *
   * @return a VBox with buttons for dealing and checking a hand of cards
   */
  private VBox createButtonDisplay() {
    cardDeck = new DeckOfCards();
    
    Button dealHandButton = new Button("Deal Hand");
    dealHandButton.setOnAction(event -> {
      cardHandDisplay.setText(dealHandButton());
    });
    
    Button checkHandButton = getCheckHandButton();
    
    Button newDeckButton = new Button("New deck");
    newDeckButton.setOnAction(event -> { cardDeck = new DeckOfCards();
    cardHandDisplay.clear();
    clearResults();
        });
    
    VBox vbox = new VBox(100, dealHandButton, checkHandButton, newDeckButton);
    vbox.setPadding(new Insets(30));
    
    return vbox;
  }
  
  public String dealHandButton() {
    String response = "";
    cardHandDisplay.clear();
    clearResults();
    if (cardDeck.getCardDeck().size() <= 5) {
      response = "Not enough cards in the deck";
    } else {
      cardDeck.dealHand(5);
      response = cardDeck.showHand();
    }
    return response;
  }
  
  /**
   * Creates a button for checking the hand of cards
   *
   * @return a button for checking the hand of cards
   */
  public Button getCheckHandButton() {
    Button checkHandButton = new Button("Check Hand");
    checkHandButton.setOnAction(event -> {
      clearResults();
      
      sumTextArea.setText(cardDeck.getLatestHand().sumCards());
      heartsTextArea.setText(cardDeck.getLatestHand().showHearts());
      flushTextArea.setText(cardDeck.getLatestHand().checkForFlush());
      queenTextArea.setText(cardDeck.getLatestHand().checkForSpadeQueen());
    });
    return checkHandButton;
  }
  
  private void clearResults() {
    sumTextArea.clear();
    heartsTextArea.clear();
    flushTextArea.clear();
    queenTextArea.clear();
  }
}
