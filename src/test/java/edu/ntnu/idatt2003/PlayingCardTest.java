package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2003.model.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
public class PlayingCardTest {
  PlayingCard card;
  
  @BeforeEach
  void setUp() {
    card = new PlayingCard('S', 1);
  }
  @Nested
  @DisplayName("Testing the constructor")
  class TestConstructor {
    @Test
    @DisplayName("Testing the constructor with a valid card")
    void testConstructor() {
      assertNotNull(card);
      assertEquals('S', card.getSuit());
      assertEquals(1, card.getFace());
    }
    @Test
    @DisplayName("Testing the constructor with an invalid suit")
    void testConstructorInvalidSuit() {
      try {
        card = new PlayingCard('A', 1);
        fail();
      } catch (IllegalArgumentException e) {
        assertEquals("Parameter suit must be one of H, D, C or S", e.getMessage());
      }
    }
    @Test
    @DisplayName("Testing the constructor with an invalid face")
    void testConstructorInvalidFace() {
      try {
        card = new PlayingCard('S', 0);
        fail();
      } catch (IllegalArgumentException e) {
        assertEquals("Parameter face must be a number between 1 to 13", e.getMessage());
      }
    }
  }
  
  @Nested
  @DisplayName("Testing the getSuit() method")
  class TestGetSuit {
    @Test
    @DisplayName("Testing the getSuit() method")
    void testGetSuit() {
      assertEquals('S', card.getSuit());
    }
  }
  
  @Nested
  @DisplayName("Testing the getFace() method")
  class TestGetFace {
    @Test
    @DisplayName("Testing the getFace() method")
    void testGetFace() {
      assertEquals(1, card.getFace());
    }
  }
  
  @Nested
  @DisplayName("Testing the getAsString() method")
  class TestGetAsString {
    @Test
    @DisplayName("Testing the getAsString() method")
    void testGetAsString() {
      assertEquals("S1", card.getAsString());
    }
  }
  
  @Nested
  @DisplayName("Testing the equals() method")
  class TestEquals {
    @Test
    @DisplayName("Testing the equals() method with two equal cards")
    void testEquals() {
      PlayingCard otherCard = new PlayingCard('S', 1);
      assertEquals(card, otherCard);
    }
    @Test
    @DisplayName("Testing the equals() method with two different cards")
    void testNotEquals() {
      PlayingCard otherCard = new PlayingCard('H', 1);
      assertNotEquals(card, otherCard);
    }
  }
}
