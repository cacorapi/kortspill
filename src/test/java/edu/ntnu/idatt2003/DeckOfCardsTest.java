package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2003.model.DeckOfCards;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class DeckOfCardsTest {
  DeckOfCards deck;
  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }
  
  @Nested
  @DisplayName("Testing methods to make, retrieve and alter the deck of cards")
  class TestDeckOfCards {
    @Test
    @DisplayName("Testing the constructor")
    void testDeckOfCards() {
      assertNotNull(deck);
      assertEquals(52, deck.getCardDeck().size());
    }
    
    @Test
    @DisplayName("Testing the getCardDeck() method")
    void testGetCardDeck() {
      assertNotNull(deck.getCardDeck());
    }
    
    @Test
    @DisplayName("Testing the selectCard method")
    void testSelectCard() {
      assertNotNull(deck.selectCard(0));
      assertEquals('S', deck.selectCard(0).getSuit());
    }
    
    @Test
    @DisplayName("Testing the deck of cards")
    void testRemoveCard() {
      deck.removeCard(0);
      assertEquals(51, deck.getCardDeck().size());
    }
  }
  
  @Nested
  @DisplayName("Testing the dealHand method")
  class TestDealHand {
    @Test
    @DisplayName("Testing if the dealHand method deals a hand of cards")
    void testDealHand() {
      deck.dealHand(5);
      assertEquals(47, deck.getCardDeck().size());
    }
    
    @Test
    @DisplayName("Testing if the dealHand method deals a hand of cards with a size of 0")
    void testDealHandZero() {
      try {
        deck.dealHand(0);
        fail();
      } catch (IllegalArgumentException e) {
        assertEquals("Cannot deal zero cards", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Testing if the dealHand method deals a hand of cards with a size of 53")
    void testDealHandTooLarge() {
      try {
        deck.dealHand(53);
        fail();
      } catch (IllegalArgumentException e) {
        assertEquals("There are not enough cards in the deck", e.getMessage());
      }
    }
  }
  
  @Nested
  @DisplayName("Testing the showHand method")
  class TestShowHand {
    @Test
    @DisplayName("Testing if the showHand method returns a string representation of the hand")
    void testShowHand() {
      deck.dealHand(5);
      StringBuilder expected = new StringBuilder();
      for (int i = 0; i < 5; i++) {
        expected.append(deck.getLatestHand().getHand().get(i).getAsString()).append(" | ");
      }
      assertEquals(expected.toString(), deck.showHand());
    }
  }
  
  @Nested
  @DisplayName("Testing the getLatestHand method")
  class TestGetLatestHand {
    @Test
    @DisplayName("Testing the getLatestHand method when a hand exists")
    void testGetLatestHand() {
      deck.dealHand(5);
      assertNotNull(deck.getLatestHand());
    }
    @Test
    @DisplayName("Testing the getLatestHand method when no hand has been dealt")
    void testGetLatestHandNoHand() {
      try {
        deck.getLatestHand();
        fail();
      } catch (IllegalArgumentException e) {
        assertEquals("No hands have been dealt", e.getMessage());
      }
    }
  }
  
}
