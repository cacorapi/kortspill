package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.assertEquals;
import edu.ntnu.idatt2003.model.CardHand;
import edu.ntnu.idatt2003.model.PlayingCard;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class CardHandTest {
  
  CardHand hand;
  
  @BeforeEach
  void setUp() {
    hand = new CardHand();
    hand.receiveCard(new PlayingCard('C', 1));
    hand.receiveCard(new PlayingCard('C', 2));
    hand.receiveCard(new PlayingCard('C', 3));
    hand.receiveCard(new PlayingCard('C', 4));
    hand.receiveCard(new PlayingCard('C', 5));
  }
  
  @Nested
  @DisplayName("Testing the getHand method")
  class TestGetHand {
    @Test
    @DisplayName("Testing if the getHand method correctly returns the hand")
    void testGetHand() {
      ArrayList<PlayingCard> expectedHand = new ArrayList<>();
      expectedHand.add(new PlayingCard('C', 1));
      expectedHand.add(new PlayingCard('C', 2));
      expectedHand.add(new PlayingCard('C', 3));
      expectedHand.add(new PlayingCard('C', 4));
      expectedHand.add(new PlayingCard('C', 5));
      
      assertEquals(expectedHand, hand.getHand());
    }
  }
    
  @Nested
  @DisplayName("Testing the receiveCard method")
  class TestReceiveCard {
    @Test
    @DisplayName("Testing if the receiveCard method adds a card to the hand")
    void testReceiveCard() {
      assertEquals(5, hand.getHand().size());
    }
    
    @Test
    @DisplayName("Testing if the receiveCard method adds a wrongly configured card to the hand")
    void testReceiveWrongCard() {
      try {
        hand.receiveCard(new PlayingCard('G', 40));
      } catch (IllegalArgumentException e) {
        assertEquals("Parameter suit must be one of H, D, C or S", e.getMessage());
      }
    }
  }
  
  @Nested
  @DisplayName("Testing the sumCards method")
  class TestSumCards {
    @Test
    @DisplayName("Testing if the sumCards method returns the correct sum of the cards in the hand")
    void testSumCards() {
      int expectedSum = 15;
      assertEquals(String.valueOf(expectedSum), hand.sumCards());
    }
  }
  
  @Nested
  @DisplayName("Testing the showHearts method")
  class TestShowHearts {
    @Test
    @DisplayName("Testing if the showHearts method returns the correct string of hearts in the hand")
    void testShowHearts() {
      hand.receiveCard(new PlayingCard('H', 1));
      hand.receiveCard(new PlayingCard('H', 2));
      String expectedHearts = "H1 | H2 | ";
      assertEquals(expectedHearts, hand.showHearts());
    }
    
    @Test
    @DisplayName("Testing if the showHearts method returns the correct string when there are no hearts in the hand")
    void testShowNoHearts() {
      assertEquals("No hearts in hand", hand.showHearts());
    }
  }
  
  @Nested
  @DisplayName("Testing the checkForSpadeQueen method")
  class TestCheckForSpadeQueen {
    @Test
    @DisplayName("Testing if the checkForSpadeQueen method returns Yes when the hand contains the queen of spades")
    void testCheckForSpadeQueenYes() {
      hand.receiveCard(new PlayingCard('S', 12));
      assertEquals("Yes", hand.checkForSpadeQueen());
    }
    
    @Test
    @DisplayName("Testing if the checkForSpadeQueen method returns No when the hand does not contain the queen of spades")
    void testCheckForSpadeQueenNo() {
      assertEquals("No", hand.checkForSpadeQueen());
    }
  }
  
  @Nested
  @DisplayName("Testing the checkForFlush method")
  class TestCheckForFlush {
    @Test
    @DisplayName("Testing if the checkForFlush method returns Yes when the hand is a flush")
    void testCheckForFlushYes() {
      assertEquals("Yes", hand.checkForFlush());
    }
    
    @Test
    @DisplayName("Testing if the checkForFlush method returns No when the hand is not a flush")
    void testCheckForFlushNo() {
      hand.receiveCard(new PlayingCard('D', 6));
      assertEquals("No", hand.checkForFlush());
    }
  }
}
